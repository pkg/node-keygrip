# Installation
> `npm install --save @types/tsscmp`

# Summary
This package contains type definitions for tsscmp (https://github.com/suryagh/tsscmp#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/tsscmp

Additional Details
 * Last updated: Fri, 28 Jun 2019 00:15:15 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Gareth Jones <https://github.com/g-rath>.
